classdef latticeSimulation < monty
    properties
        %simulation parameters and their default or initial values
        
        %inputs
        VNR           = 2;
        lattice       = En(8);
        
        %outputs
        nIntegerError = 0;
        nWordError    = 0;
        nWord         = 0;
        
        %simulation parameters without defaults
        varChan;
        ier          = [] ;
        wer          = [] ;
    end
    
    properties (Hidden)
        %values that are computed on initialization, but do not need to be
        %displayed.
    end
    
    methods
        function curve = latticeSimulation(varargin)
            %continue condition: if string evaluates true, then the
            %simulation will continue.  Otherwise, the simulation stops
            curve.montyContinue    = 'curve.nWordError < 10';
            curve.montyAddField    = {'nIntegerError','nWordError','nWord'};
            curve.montyConcatField = { };
            
            %process the input arguments, for example allows
            %latticeSimulation('lattice',L) to override default 'lattice' value
            curve = curve.inputArguments(varargin{:});
        end
        
        function curve = simulate(curve)
            
            %L and n mean curve.lattice and curve.lattice.n
            L = curve.lattice;
            n = curve.lattice.n;
            
            %SIMULATION CORE
            curve.varChan = L.vnrToVariance(curve.VNR);
            while (curve.montyErrorLimit)
                b = randi(100,n,1) - 50;
                x = L.G * b;
                z = randn(n,1) * sqrt(curve.varChan);
                y = x + z;
                
                bhat = round(L.H * L.decoder(y));
                
                nie                 = length(find(bhat ~= b));
                curve.nIntegerError = curve.nIntegerError + nie;
                curve.nWordError    = curve.nWordError + (nie > 0);
                curve.nWord         = curve.nWord + 1;
            end
            
            %Convenience calculations
        end
        
        function curve = functionExit(curve)
            %This function is called after simulations are complete.
            %Particularly important when using parallel processing.
            curve.ier = curve.nIntegerError / (curve.nWord * curve.lattice.n);
            curve.wer = curve.nWordError / curve.nWord ;
        end
        
    end
    
end
