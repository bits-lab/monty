# Monty - Matlab Library for Managing Communications Simulations

Monty is a Matlab object-oriented library for efficient simulations of communication systems, particularly generating a curve of  probability of decoder error versus signal-to-noise ratio.  A goal is to obtain statistically reliable data with efficient use of computer time.  If your simulation is written as a monty subclass, then Monty is used to begin a simulation, and to stop the simulation when a specified number of errors have occurred.  The Matlab parallel processing toolbox with parfor, remote servers and PBS job management software are supported.  Monty allows safely resuming simulations after a server crash.

Monty uses Matlab classes.  It is based on the older Curve Library, which used functions rather than classes. Because of the benefits of a class-based library, development on Curve Library has stopped, and use of Monty is recommended.  Even if you are unfamiliar with Matlab classes, you should be able to write your simulation as a class by modifying the provided examples.   Communications simulations are an example of a Monte Carlo simulation, hence the library name "Monty."

See LICENSE for more information.

## Documentation

Full documentation is in doc/monty.pdf.

## Installation

Distribution is by Gitlab:

   > $ git clone https://gitlab.com/bits-lab/monty.git

git clone also creates a large and unneeded .git directory.  Remote this using rm -rf .git

Or download from https://gitlab.com/bits-lab/monty/tree/master

## Revision History

2020 Jul 8 - Started fixing a critical bug -- number of errors and frames were incorrectly increased after first time parallel processing ran

2019 Jun 7 - Works with new JAIST VPCC server

2018 Feb 17 - Added method inputArguments to monty, examples and documentation.

2018 Feb 14 - Major updates.

2018 Jan 11 - Initial release.
