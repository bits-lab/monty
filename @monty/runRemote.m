function runRemote(inputString,varargin)
%runRemote - Run a simulation on a remote server
%
%   This function requires configuration variables in VAR/MONTYCONFIG*.M
%   are set properly.
%
%   On Mac and Linux systems, RUNREMOTE will upload files in the
%   current directory to the remote server, and runs the specified program.
%   Command-line utilties rsync and ssh must be present. On Windows systems,
%   the user must manually upload files, and remote execution requires
%   putty.exe.
%
%   There are two operation modes.  In both modes, the directory tree is
%   copied to the remote server using rsync.
%
%   RUNREMOTE('simufile')  If SIMUFILE.MAT exists in the current
%   directory, and contains a structure CURVE, then the remote Matlab
%   command is monty.run('simufile'), running the simulation.
%
%   RUNREMOTE('command')  If the first argument is not a MAT file,
%   then it is treated as a Matlab command to be run on the remote server.
%   This mode does not use the Monty Library, but is a convenient way to
%   remotely execute Matlab commands. For example, the following computes
%   the eigenvalues of a random matrix, and saves the result to a file.
%
%      cmd = '[V,D] = eig(rand(100)); save mydata V D;';
%      monty.runRemote(cmd);
%
%   RUNREMOTE(...,'jobname') changes the job name from default of
%   'userjob' to JOBNAME.  This is useful when running multiple jobs in
%   the same directory.
%
%   RUNREMOTE(...,'parallel') or RUNREMOTE(...,'p') will attempt to open 
%   and use the parallel pool. Matlab's parallel processing toolbox is 
%   required.  The 'parallel' option is after the jobname.  
%
%   Since simulations nomially save data to files on the remote server,
%   these files should be copied to the local machine using MONTY.FROMREMOTE.
%
%   See also MONTY.FROMREMOTE
%
%   Brian Kurkoski 2016, 2018


if nargin < 1
    error('requires one input argument')
elseif nargin == 1
    parallelFlag = false;
    jobname = monty.defaultJobname;
elseif nargin == 2
    %check for parallel flag
    if strcmpi(varargin{1},'parallel') || strcmpi(varargin{1},'p')
        parallelFlag = true;
        jobname = monty.defaultJobname;
    else
        parallelFlag = false;
        jobname = varargin{1};
    end
elseif nargin == 3
    assert(strcmpi(varargin{2},'parallel') || strcmpi(varargin{2},'p'),'when using 3 inputs, last one must be ''parallel''');
    parallelFlag = true;
    jobname = varargin{1};
    
else
    error('maximum 3 input arguments');
end

try
    %input is a MAT filename
    loadFile = true;
    H = load(inputString);
catch
    %input is a remote Matlab command
    loadFile = false;
end

if loadFile
    fprintf('Input is a MAT file\n')
    assert(isfield(H,'curve'),'File %s.MAT does not contain CURVE\n',upper(inputString));
    assert(isobject(H.curve),'CURVE in file %s.MAT is not a structure\n',upper(inputString));
    fprintf('curve(1).montyContinue = %s\n',H.curve(1).montyContinue);
    %remoteMatlabCommand = sprintf('curveSimu(''%s'');',inputString);
    
    if parallelFlag
        fprintf('parallel flag = true\n');
        remoteMatlabCommand = sprintf('monty.run(''%s'',''parallel'');',inputString);
    else
        fprintf('parallel flag = false\n');
        remoteMatlabCommand = sprintf('monty.run(''%s'');',inputString);
    end
else
    fprintf('Input is a Matlab command\n')
    remoteMatlabCommand = inputString;
end

config = monty.remoteLoadConfiguration;

pbsfilename = [ monty.configDir '/'  jobname  '.pbs'];

FL = fopen(pbsfilename,'w');
%Old JAIST HPCC until February 2018 
%fprintf(FL,'#!/bin/sh\n\n');
%New JAIST VPCC
fprintf(FL,'#!/bin/csh\n\n');
fprintf(FL,'#PBS -l select=1\n');

if isfield(config,'pbsQueueClass')
    fprintf(FL,'#PBS -q %s\n',config.pbsQueueClass);
end

fprintf(FL,'#PBS -j oe\n');
fprintf(FL,'#PBS -e %s/%s.err\n',monty.configDir,jobname);
fprintf(FL,'#PBS -o %s/%s.out\n',monty.configDir,jobname);
fprintf(FL,'#PBS -N %s\n',jobname);
fprintf(FL,'cd $PBS_O_WORKDIR\n');
%Old JAIST HPCC until February 2018 
%fprintf(FL,'export MATLABROOT=/opt/matlab_R2017b\n');  
%fprintf(FL,'export PATH="$PATH:$MATLABROOT/bin"\n');
%New JAIST VPCC
fprintf(FL,'module load app_matlab\n');
fprintf(FL,'matlab -nodisplay -r "%s" \n',remoteMatlabCommand);
fclose(FL);

fprintf('Created file %s\n',pbsfilename);

%copy additional files
if ~ispc
    if isfield(config,'shellCommand')
        error('shellCommand was changed to localShellCommand.  Update configureRemote.m');
    end
    if isfield(config,'localShellCommand') && ~isempty(config.localShellCommand)
        fprintf('Local shell command in config file\n');
        %fprintf('Shell command: %s\n',config.shellCommand);
        status = system(config.localShellCommand);
        if status
            error('Shell command on local machine failed');
        end
    end
end

%rsync files to host
if ~ispc
    fprintf('Rsync files to %s\n',upper(config.servername));
    %--update is important if a simulation is already running in same dir
    cmd    = sprintf('rsync -ae ssh --update . %s@%s:%s',config.username,config.servername,config.remoteDir);
    status = system(cmd);
    if status
        error('rsync to server failed');
    end
else
    fprintf('WINDOWS: Copy files to server manually.  When done,\n')
    reply = input('run command on server? y/n [y]:','s');
    if isempty(reply)
        reply = 'y';
    end
    if ~strcmpi(reply,'y')
        fprintf('Do nothing\n')
        return
    end
end


%qsub to submit PBS job

if ~ispc
    if config.usePBS
        fprintf('Remote command submits PBS job: %% qsub pbs/%s.pbs\n',jobname);
        cmd = sprintf('ssh %s@%s "cd %s ; qsub %s/%s.pbs"',config.username,config.servername,config.remoteDir,monty.configDir,jobname);
    else
        fprintf('Run MATLAB direct\n');
        %some versions require "source ~/.bashrc ; source ~/.bash_profile"
        %THIS IS AN EXAMPLE OF THE REMOTE COMMAND WE WANT TO EXECUTE
        %ssh kurkoski@150.65.124.11 "source ~/.bashrc ; source ~/.bash_profile ; cd ~/myProject ; LANG=en_GB.utf-8 ; matlab -nodisplay -r 'disp(pwd) ; disp(date)'   > var/userjob.out 2> var/userjob.err & "
        %THIS IS A SIMPLER VERSION
        %ssh kurkoski@150.65.124.11 "cd ~/myProject ; matlab -nodisplay -r 'disp(pwd) ; disp(date)' > var/userjob.out 2> var/userjob.err & "
        cmd = sprintf('ssh %s@%s "cd %s ; matlab -nodisplay -r ''%s'' > var/%s.out 2> var/%s.out & "',...
            config.username,config.servername,config.remoteDir,remoteMatlabCommand,jobname,jobname);
    end
    [status,result] = system(cmd);
    if status
        error('submit PBS job failed');
    else
        jobID = regexp(result,'\d+','match');
    end
else
    FL = fopen('servernameCommands','w')
    if config.usePBS
        %fprintf('Submit PBS job: %%qsub var/%s.pbs',upper(jobname));
        fprintf(FL,'"cd %s ; qsub var/%s.pbs"\n',config.remoteDir,jobname);
    else
        %other candidates
        % old: matlab -nojvm < G3_n100_d3.m > stdout2.txt 2> stderr2.txt &
        % old: matlab  -nodesktop -nodisplay -r "simu('data/G3_n100d5_longer');" &
        % old: matlab -nojvm -r "simu('data/G3_n100d5_longer');" & > stdout.txt 2> stderr.txt
        fprintf('Run MATLAB direct\n');
        fprintf(FL,'cd %s ; matlab -nodisplay -r ''%s'' > var/%s.out 2> var/%s.out & \n',...
            config.remoteDir,remoteMatlabCommand,jobname,jobname);
    end
    
    fclose(FL);
    
    cmd = sprintf('%s %s@%s -m servernameCommands',config.puttyPath,config.username,config.servername);
    [status,result] = system(cmd);
    if status
        error('submit PBS job failed');
    else
        jobID = regexp(result,'\d+','match');
    end
end

if ~isempty(jobID)
    if nargout == 0
        fprintf('Job ID is %s. Useful commands:\n >> monty.remoteShell(''qstat %s'') %%get status on server\n >> monty.fromRemote  %%copy files from server to local\n >> monty.remoteShell(''qdel %s'')   %%delete job\n >> monty.qstat %%list jobs owned by %s\n',jobID{1},jobID{1},jobID{1},config.username);
    end
end

%if possible, get output which includes job number.
%     printf("line 0 : $output[0]\n");
%     preg_match("/[^\d]{0,}(\d*)/", $output[0] , $matches);
%     printf("Job number: $matches[1]");
%useful?
%runphpFile = mfilename('fullpath');

return



